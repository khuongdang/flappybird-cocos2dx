#include "GameOverScene.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "Deffinition.h"

USING_NS_CC;

unsigned int Score=0;

unsigned int HighScore = 0;

Scene* GameOverScene::createScene(unsigned int _Score)
{
	Score = _Score;

    return GameOverScene::create();
}

// on "init" you need to initialize your instance
bool GameOverScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	Sprite* BackGround = Sprite::create("Background.jpg");
	BackGround->setPosition(visibleSize.width / 2 + origin.y, visibleSize.height / 2 + origin.y);
	this->addChild(BackGround);

	String* ScoreString = String::createWithFormat("%i", Score);

	Label* ScoreLabel = Label::createWithSystemFont(ScoreString->getCString(), "arial",GAMEOVER_SCORE_HEIGHT*visibleSize.height);
	ScoreLabel->setColor(Color3B::YELLOW);
	ScoreLabel->enableBold();
	ScoreLabel->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

	this->addChild(ScoreLabel);

	EventListenerTouchOneByOne* TouchListener = EventListenerTouchOneByOne::create();
	TouchListener->setSwallowTouches(true);
	TouchListener->onTouchBegan = CC_CALLBACK_2(GameOverScene::OnTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(TouchListener, this);

	HighScore = UserDefault::getInstance()->getIntegerForKey("HighScore", 0);
	if (HighScore < Score)
	{
		HighScore = Score;
		UserDefault::getInstance()->setIntegerForKey("HighScore", Score);
	}

	String* HighScoreString = String::createWithFormat("%i", HighScore);

	Label* HighScoreLabel = Label::createWithSystemFont(HighScoreString->getCString(), "arial", GAMEOVER_SCORE_HEIGHT*visibleSize.height/2);
	HighScoreLabel->setColor(Color3B::WHITE);
	HighScoreLabel->enableBold();
	HighScoreLabel->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height*3 / 4 + origin.y);

	this->addChild(HighScoreLabel);

    return true;
}

bool GameOverScene::OnTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
	Director::getInstance()->replaceScene(TransitionFade::create(0.5, GameScene::createScene()));
	return true;
}


void GameOverScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}
