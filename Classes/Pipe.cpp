#include "Pipe.h"
#include "Deffinition.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Pipe::Pipe()
{
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

}

void Pipe::SpawnPipe(cocos2d::Scene *scene,float PipeGap)
{
	//Init Variable
	Vec2 MoveVector = Vec2(-PIPE_MOVE_SPEED*visibleSize.width, 0);


	//Init Bot_Pipe
	Sprite* Bot_PipeSprite = Sprite::create("Pipe.png");
	Size SpriteSize = Bot_PipeSprite->getContentSize();

	//Init Height
	float Spawnable_Height = visibleSize.height*(1 - REAL_GROUND_HEIGHT);
	float Random = CCRANDOM_0_1()/2+0.15;

	if (Random > UPPER_SPAWN_THRESTHOLD)
		Random = UPPER_SPAWN_THRESTHOLD;
	else if (Random < LOWER_SPAWN_THRESTHOLD)
		Random = LOWER_SPAWN_THRESTHOLD;

	float Spawn_Height = Spawnable_Height * Random - SpriteSize.height/2;

	float PipepositionY = visibleSize.height*REAL_GROUND_HEIGHT + Spawn_Height + origin.y;
	//

	float PipepositionX = visibleSize.width + SpriteSize.width/2 + origin.x; //outside of the scene

	Bot_PipeSprite->setPosition(Vec2(PipepositionX, PipepositionY));

	PhysicsBody *Bot_PipePhysic = PhysicsBody::createEdgeBox( SpriteSize);
	Bot_PipePhysic->setGravityEnable(false);
	Bot_PipePhysic->setDynamic(false);
	Bot_PipePhysic->setVelocity(MoveVector);
	Bot_PipePhysic->setCategoryBitmask(OBSTACK_BITMASK);
	Bot_PipePhysic->setContactTestBitmask(true);

	Bot_PipeSprite->setPhysicsBody(Bot_PipePhysic);
	;
	//

	//Init TopPipe
	Sprite* Top_PipeSprite = Sprite::create("Pipe.png");
	Top_PipeSprite->setScaleY(-1);
	Top_PipeSprite->setPosition(PipepositionX,PipepositionY + PipeGap + SpriteSize.height);

	PhysicsBody *Top_PipePhysic = PhysicsBody::createEdgeBox(SpriteSize);
	Top_PipePhysic->setGravityEnable(false);
	Top_PipePhysic->setDynamic(false);
	Top_PipePhysic->setVelocity(MoveVector);
	Top_PipePhysic->setCategoryBitmask(OBSTACK_BITMASK);
	Top_PipePhysic->setContactTestBitmask(true);

	Top_PipeSprite->setPhysicsBody(Top_PipePhysic);
	//

	//Init ScoreBody
	PhysicsBody *ScoreBody = PhysicsBody::createBox(Size(1, PipeGap));
	ScoreBody->setGravityEnable(false);
	ScoreBody->setDynamic(false);
	ScoreBody->setVelocity(MoveVector);
	ScoreBody->setCategoryBitmask(SCORE_BITMASK);
	ScoreBody->setContactTestBitmask(true);

	Node *ScoreNode = Node::create();
	ScoreNode->setPosition(PipepositionX, PipepositionY + PipeGap / 2 + SpriteSize.height / 2);

	ScoreNode->setPhysicsBody(ScoreBody);
	//

	//Group Sprites


	// Show Sprites
	scene->addChild(Bot_PipeSprite);
	scene->addChild(Top_PipeSprite);
	scene->addChild(ScoreNode);
}
