#include "SplashScene.h"
#include "MenuScene.h"
#include "Deffinition.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* SplashScene::createScene()
{
    return SplashScene::create();
}

// on "init" you need to initialize your instance
bool SplashScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	this->scheduleOnce(schedule_selector(SplashScene::GoToMainMenuScene), DISPLAY_TIME_SPLASH_SCENE);

	Label* SplashSceneLabel = Label::createWithSystemFont("KHUONGDANG\nPRODUCTION", "arial", SPLASH_SCENE_FONT_HEIGHT*visibleSize.height);
	SplashSceneLabel->setColor(Color3B::BLUE);
	SplashSceneLabel->enableBold();
	SplashSceneLabel->setPosition(visibleSize.width/2 + origin.x, visibleSize.height / 2 + origin.y);

	this->addChild(SplashSceneLabel);

	// Add Sound
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/Wing.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/Point.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/Hit.mp3");
	//

    return true;
}

void SplashScene::GoToMainMenuScene( float dt)
{
	auto scene = MenuScene::createScene();
	
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}


void SplashScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}
