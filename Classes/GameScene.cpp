#include "GameScene.h"
#include "GameOverScene.h"
#include "SimpleAudioEngine.h"

#include "Deffinition.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
	Scene* scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	scene->getPhysicsWorld()->setGravity(Vec2(0, FALL_SPEED*Director::getInstance()->getVisibleSize().height));

    Scene* Layer = GameScene::create();

	scene->addChild(Layer);

	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();




	// Add BackGround

	Sprite* BackGroundSprite = Sprite::create("BackGround.jpg");
	BackGroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	BackGroundSprite->setScaleX(visibleSize.width /BackGroundSprite->getContentSize().width);
	BackGroundSprite->setScaleY(visibleSize.height / BackGroundSprite->getContentSize().height);

	this->addChild(BackGroundSprite);

	Sprite* GroundSprite = Sprite::create("Ground.jpg");
	GroundSprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height*REAL_GROUND_HEIGHT/2 + origin.y ));
	GroundSprite->setScaleX(visibleSize.width / GroundSprite->getContentSize().width);
	GroundSprite->setScaleY(visibleSize.height*REAL_GROUND_HEIGHT / GroundSprite->getContentSize().height);

	this->addChild(GroundSprite, 100);
	//

	//Create EdgephysicBody
	
	PhysicsBody* EdgeBody = PhysicsBody::createEdgeBox(Size(visibleSize.width, visibleSize.height*(1 - REAL_GROUND_HEIGHT)));
	EdgeBody->setGravityEnable(false);
	EdgeBody->setPositionOffset(Vec2(0, GROUND_GAP*visibleSize.height));
	EdgeBody->setCategoryBitmask(OBSTACK_BITMASK);
	EdgeBody->setContactTestBitmask(true);

	
	Node* EdgeNode = Node::create();
	EdgeNode->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	EdgeNode->setPhysicsBody(EdgeBody);

	this->addChild(EdgeNode);
	//

	// Init the Bird and touch

	bird = new Bird(this);

	EventListenerTouchOneByOne* TouchListener = EventListenerTouchOneByOne::create();
	TouchListener->setSwallowTouches(true);
	TouchListener->onTouchBegan = CC_CALLBACK_2(GameScene::OnTouchBegan,this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(TouchListener, this);

	//

	// Spawn Pipe

	this->schedule(schedule_selector(GameScene::SpawnPipe), PIPE_SPAWN_DISTAINT*visibleSize.width);

	//

	//  Contactlistener
	EventListenerPhysicsContact* ContactListener = EventListenerPhysicsContact::create();
	ContactListener->onContactBegin = CC_CALLBACK_1(GameScene::OnConTactBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(ContactListener, this);

	//

	// Create Score Label

	String* ScoreString = String::createWithFormat("%i", Score);

	ScoreLabel = Label::createWithSystemFont(ScoreString->getCString(), "Arial", SCORE_HEIGHT*visibleSize.height);
	ScoreLabel->setColor(Color3B::YELLOW);
	ScoreLabel->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height *0.9 + origin.y));
	ScoreLabel->enableOutline(Color4B::BLACK, 1);

	this->addChild(ScoreLabel,100);

	//

	
	this->scheduleUpdate();

	bird->Fly();

    return true;
}

void GameScene::update(float dt)
{

}

bool GameScene::OnTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
	bird->Fly();
	return true;
}

void GameScene::SpawnPipe(float dt)
{
	pipe.SpawnPipe(this,bird->GetSize()*PIPE_GAP);
}

bool GameScene::OnConTactBegan(cocos2d::PhysicsContact &contact)
{
	float aBismask = contact.getShapeA()->getBody()->getCategoryBitmask();
	float bBismask = contact.getShapeB()->getBody()->getCategoryBitmask();

	
	if ((aBismask == BIRD_BITMASK && bBismask == OBSTACK_BITMASK) || (bBismask == BIRD_BITMASK && aBismask == OBSTACK_BITMASK))
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Hit.mp3");
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, GameOverScene::createScene(Score)));
	}
	else if ((aBismask == BIRD_BITMASK && bBismask == SCORE_BITMASK)||(bBismask == BIRD_BITMASK && aBismask == SCORE_BITMASK))
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Point.mp3");
		Score++;
		String* ScoreString = String::createWithFormat("%i", Score);
		ScoreLabel->setString(ScoreString->getCString());
	}
	return true;
}

void GameScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}
