#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H_

#include "cocos2d.h"
#include "Bird.h"
#include "Pipe.h"

class GameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback

    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);

private:
	Bird *bird;

	Pipe pipe;

	cocos2d::Label *ScoreLabel;

	void SpawnPipe(float dt);

	void update(float dt);

	bool OnTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

	bool OnConTactBegan(cocos2d::PhysicsContact &contact);
	
	int Score = 0;
	

};

#endif // !__GAME_SCENE_H__
