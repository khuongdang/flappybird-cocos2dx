#include "SimpleAudioEngine.h"
#include "Bird.h"

#include "Deffinition.h"

USING_NS_CC;

Bird::Bird(cocos2d::Scene *scene)
{
	VisibleSize = Director::getInstance()->getVisibleSize();
	Origin = Director::getInstance()->getVisibleOrigin();

	BirdSprite = Sprite::create("Bird.png");
	BirdSprite->setPosition(Point(VisibleSize.width / 2 + Origin.x, VisibleSize.height / 2 + Origin.y));

	BirdSize = BirdSprite->getContentSize().height;

	auto BirdBody = PhysicsBody::createCircle(BirdSize / 2);
	BirdBody->setCategoryBitmask(BIRD_BITMASK);
	BirdBody->setCollisionBitmask(BIRD_COLLISION_BITMASK);	
	BirdBody->setContactTestBitmask(BIRD_CONTACT_BITMASK);
	BirdSprite->setPhysicsBody(BirdBody);

	scene->addChild(BirdSprite, 100);
}

float Bird::GetSize()
{
	return BirdSize;
}

void Bird::Fly()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Wing.mp3");
	float Flyspeed = FLY_SPEED*VisibleSize.height;
	BirdSprite->getPhysicsBody()->setVelocity(Vec2(0, Flyspeed ));
	Animate();
}

void Bird::Animate()
{	
	float rotatetime = ROTATE_TIME*(-FALL_SPEED/FLY_SPEED);

	Rect SpriteRec = Rect(Vec2(), BirdSprite->getContentSize());
	Vector<SpriteFrame*> FlapSprite;
	FlapSprite.pushBack(SpriteFrame::create("BirdFlap.png", SpriteRec));
	FlapSprite.pushBack(SpriteFrame::create("Bird.png", SpriteRec));
	Animation* Flap = Animation::createWithSpriteFrames(FlapSprite,FLAP_ANIMATION_DURATION);
	cocos2d::Animate* animateFlap = Animate::create(Flap);

	RotateTo* rotateup = RotateTo::create(rotatetime, -ROTATE_ANGLE);
	RotateTo* rotatedown = RotateTo::create(rotatetime, ROTATE_ANGLE);
	
	Sequence* Action = Sequence::create(rotateup, rotatedown,NULL);


	BirdSprite->runAction(animateFlap);
	BirdSprite->runAction(Action);
}

int Bird::isFly()
{
	if (isClicked == true)
	{
		isClicked = false;
		return 1;
	}
	else if (BirdSprite->getPhysicsBody()->getVelocity().y <= 0.0)
		return -1;
	else return 0;
}






