#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"

class GameOverScene: public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene(unsigned int _Score);

	bool OnTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameOverScene);

private:
	
};

#endif // __GAMEOVER_SCENE_H__
