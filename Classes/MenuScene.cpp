#include "MenuScene.h"
#include "GameScene.h"
#include "Deffinition.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* MenuScene::createScene()
{
    return MenuScene::create();
}

// on "init" you need to initialize your instance
bool MenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto BackGroundSprite = Sprite::create("BackGround.jpg");
	BackGroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	BackGroundSprite->setScaleX(visibleSize.width / BackGroundSprite->getContentSize().width);
	BackGroundSprite->setScaleY(visibleSize.height / BackGroundSprite->getContentSize().height);

	Sprite* GroundSprite = Sprite::create("Ground.jpg");
	GroundSprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height*REAL_GROUND_HEIGHT / 2 + origin.y));
	GroundSprite->setScaleX(visibleSize.width / GroundSprite->getContentSize().width);
	GroundSprite->setScaleY(visibleSize.height*REAL_GROUND_HEIGHT / GroundSprite->getContentSize().height);

	this->addChild(GroundSprite, 100);

	this->addChild(BackGroundSprite);

	auto Bird = Sprite::create("Bird.png");
	Bird->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	this->addChild(Bird);


	auto TouchListener = EventListenerTouchOneByOne::create();
	TouchListener->setSwallowTouches(true);
	TouchListener->onTouchBegan = CC_CALLBACK_2(MenuScene::GoToGameScene, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(TouchListener, this);

    return true;
}

bool MenuScene::GoToGameScene(cocos2d::Touch *touch, cocos2d::Event *event)
{
	auto scene = GameScene::createScene();

	Director::getInstance()->replaceScene(scene);
	return true;
}


void MenuScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}
