#ifndef __DEFINITION_H__
#define __DEFINITION_H__

#define REAL_GROUND_HEIGHT 0.15
#define GROUND_GAP REAL_GROUND_HEIGHT/2

#define DISPLAY_TIME_SPLASH_SCENE 2
#define TRANSITION_TIME 0.5
#define SPLASH_SCENE_FONT_HEIGHT 0.05

#define FALL_SPEED -0.7
#define FLY_SPEED 0.4

#define ROTATE_TIME 0.3
#define ROTATE_ANGLE 35

#define PIPE_SPAWN_DISTAINT 0.01
#define PIPE_MOVE_SPEED 0.2

#define UPPER_SPAWN_THRESTHOLD 0.65
#define LOWER_SPAWN_THRESTHOLD 0.15

#define BIRD_BITMASK 0x1 << 0				//0001
#define OBSTACK_BITMASK 0x1 << 1			//0010
#define SCORE_BITMASK 0x1 << 2				//0100

#define BIRD_COLLISION_BITMASK 0x2			//0010

#define BIRD_CONTACT_BITMASK 0x6			//0110

#define SCORE_HEIGHT 0.1

#define GAMEOVER_SCORE_HEIGHT 0.2

#define PIPE_GAP 4

#define FLAP_ANIMATION_DURATION 0.25



#endif // !__DEFINITION__
