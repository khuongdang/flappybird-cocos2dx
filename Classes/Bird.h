#ifndef __BIRD_H__
#define __BIRD_H__

#include "cocos2d.h"

class Bird
{
public:
	Bird(cocos2d::Scene *scene);
private:
	bool isClicked;
	float BirdSize;
	cocos2d::Size VisibleSize;
	cocos2d::Vec2 Origin;
	cocos2d::Sprite *BirdSprite;
public:
	void Animate();
	int isFly();
	void Fly();
	float GetSize();
};

#endif // __HELLOWORLD_SCENE_H__
